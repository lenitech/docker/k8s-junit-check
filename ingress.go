package main

import (
	"bytes"
	"fmt"
	"github.com/jstemmer/go-junit-report/v2/junit"
	"net/http"
	"net/url"
	"time"
)

type Ingress struct {
	ApiVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   Metadata
	Spec       IngressSpec `yaml:"spec"`
}

type IngressSpec struct {
	IngressClassName string        `yaml:"ingressClassName"`
	Rules            []IngressRule `yaml:"rules"`
}

type IngressRule struct {
	Host string `yaml:"host"`
	Http struct {
		Paths []IngressPath `yaml:"paths"`
	}
}

type IngressPath struct {
	Path string `yaml:"path"`
}

func (i *Ingress) Test(s *junit.Testsuite) {
	defer wg.Done()
	for _, rule := range i.Spec.Rules {
		t := junit.Testcase{
			Name:      rule.Host,
			Classname: "ingress",
		}
		timeout, _ := time.ParseDuration("30s")
		client := &http.Client{
			Timeout: timeout,
		}
		start := time.Now()
		reqUrl, err := url.Parse(fmt.Sprintf("https://%s", rule.Host))
		if err != nil {
			t.Failure = &junit.Result{
				Message: "failure",
				Type:    "unknown",
				Data:    fmt.Sprintf("%v", err),
			}
			s.AddTestcase(t)
			continue
		}
		reqUrl = reqUrl.JoinPath(rule.Http.Paths[0].Path)
		r, err := client.Get(reqUrl.String())
		t.Time = fmt.Sprintf("%f", time.Since(start).Seconds())
		if err != nil {
			t.Failure = &junit.Result{
				Message: "failure",
				Type:    "unknown",
				Data:    fmt.Sprintf("%v", err),
			}
			s.AddTestcase(t)
			continue
		}
		esc := 200
		if _, ok := i.Metadata.Annotations["nginx.ingress.kubernetes.io/auth-tls-verify-client"]; ok {
			esc = 400
		}
		if _, ok := i.Metadata.Annotations["nginx.ingress.kubernetes.io/whitelist-source-range"]; ok {
			esc = 403
		}
		if r.StatusCode != esc {
			t.Error = &junit.Result{
				Message: r.Status,
				Type:    "http",
				Data:    fmt.Sprintf("%v", r.Status),
			}
		}
		t.SystemErr = &junit.Output{
			Data: fmt.Sprintf("%v", r.Header),
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(r.Body)
		t.SystemOut = &junit.Output{
			Data: fmt.Sprintf("%s", string(buf.String())),
		}
		s.AddTestcase(t)
	}
}
