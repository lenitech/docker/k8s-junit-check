module gitlab.com/lenitech/docker/k8s-junit-check

go 1.19

require (
	github.com/jstemmer/go-junit-report/v2 v2.1.0
	github.com/xanzy/go-gitlab v0.95.2
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.5 // indirect
	golang.org/x/oauth2 v0.16.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
)
