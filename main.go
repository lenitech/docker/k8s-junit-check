package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"github.com/jstemmer/go-junit-report/v2/junit"
	"gopkg.in/yaml.v3"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"
)

var wg sync.WaitGroup

func init() {
	err := GitlabInit(os.Getenv("CI_API_V4_URL"), os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}

func main() {
	t := junit.Testsuites{}
	ti := junit.Testsuite{
		Name: "ingress",
	}
	root := "."
	if len(os.Args) >= 2 {
		root = os.Args[len(os.Args)-1]
	}
	fmt.Fprintf(os.Stderr, "Parse files from %s\n", root)
	start := time.Now()
	err := filepath.Walk(root, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if path.Ext(p) == ".yml" || path.Ext(p) == ".yaml" {
			f, err := os.ReadFile(p)
			if err != nil {
				return err
			}
			dec := yaml.NewDecoder(bytes.NewReader(f))
			for {
				i := Ingress{}
				if dec.Decode(&i) != nil {
					break
				}
				if i.Kind == "Ingress" {
					wg.Add(1)
					go i.Test(&ti)
				}
			}
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	wg.Wait()
	ti.Time = fmt.Sprintf("%f", time.Since(start).Seconds())
	t.AddSuite(ti)
	b, err := xml.MarshalIndent(t, "", "    ")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	fmt.Fprintf(os.Stdout, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n%s\n", string(b))
	pid := os.Getenv("CI_PROJECT_ID")
	if pid != "" {
		for _, ts := range t.Suites {
			for _, tc := range ts.Testcases {
				if tc.Error != nil || tc.Failure != nil {
					err := OpenGitlabIncident(&tc, pid)
					if err != nil {
						fmt.Fprintf(os.Stderr, "%v\n", err)
						os.Exit(1)
					}
				} else {
					err := CloseGitlabIncident(&tc, pid)
					if err != nil {
						fmt.Fprintf(os.Stderr, "%v\n", err)
						os.Exit(1)
					}
				}
			}
		}
	}
	os.Exit(0)
}
