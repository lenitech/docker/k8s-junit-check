package main

import (
	"fmt"
	"github.com/jstemmer/go-junit-report/v2/junit"
	"github.com/xanzy/go-gitlab"
	"os"
)

var (
	client *gitlab.Client
)

func GitlabInit(url string, token string) (err error) {
	client, err = gitlab.NewClient(token, gitlab.WithBaseURL(url))
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to create Gitlab client: %v\n", err)
		return err
	}
	return nil
}

func OpenGitlabIncident(tc *junit.Testcase, pid string) (err error) {
	var title string
	var desc string
	var issue *gitlab.Issue
	if tc.Failure != nil {
		title = fmt.Sprintf("[failure][%s][%s][%s] %s", tc.Classname, tc.Name, tc.Failure.Type, tc.Failure.Message)
		desc = (fmt.Sprintf("# Failure\n\n- ClassName: **%s**\n- Domain: **https://%s**\n- Type: **%s**\n- Message: **%s**\n\n~~~\n%s\n~~~", tc.Classname, tc.Name, tc.Failure.Type, tc.Failure.Message, tc.Failure.Data))
	} else if tc.Error != nil {
		title = fmt.Sprintf("[error][%s][%s][%s] %s", tc.Classname, tc.Name, tc.Error.Type, tc.Error.Message)
		desc = (fmt.Sprintf("# Error\n\n- ClassName: **%s**\n- Domain: **https://%s**\n- Type: **%s**\n- Message: **%s**\n\n~~~\n%s\n~~~", tc.Classname, tc.Name, tc.Error.Type, tc.Error.Message, tc.Error.Data))
	}
	issues, _, err := client.Issues.ListProjectIssues(pid, &gitlab.ListProjectIssuesOptions{
		IssueType: func(s string) *string { return &s }("incident"),
		State:     func(s string) *string { return &s }("opened"),
		In:        func(s string) *string { return &s }("title"),
		Search:    func(s string) *string { return &s }(title),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to list Gitlab issues: %v\n", err)
		return err
	}
	if len(issues) <= 0 {
		issue, _, err = client.Issues.CreateIssue(pid, &gitlab.CreateIssueOptions{
			Title:       func(s string) *string { return &s }(title),
			Description: func(s string) *string { return &s }(desc),
			IssueType:   func(s string) *string { return &s }("incident"),
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to create Gitlab issue: %v\n", err)
			return err
		}
	} else {
		issue = issues[0]
	}
	_, _, err = client.Notes.CreateIssueNote(issue.ProjectID, issue.IID, &gitlab.CreateIssueNoteOptions{
		Body: func(s string) *string { return &s }(fmt.Sprintf("## Headers\n\n~~~\n%v\n~~~\n\n## Body\n\n~~~\n%s\n~~~", tc.SystemErr.Data, tc.SystemOut.Data)),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to create Gitlab note: %v\n", err)
		return err
	}
	return nil
}

func CloseGitlabIncident(tc *junit.Testcase, pid string) (err error) {
	issues, _, err := client.Issues.ListProjectIssues(pid, &gitlab.ListProjectIssuesOptions{
		IssueType: func(s string) *string { return &s }("incident"),
		State:     func(s string) *string { return &s }("opened"),
		In:        func(s string) *string { return &s }("title"),
		Search:    func(s string) *string { return &s }(fmt.Sprintf("[%s][%s]", tc.Classname, tc.Name)),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to list Gitlab issues: %v\n", err)
		return err
	}
	if len(issues) > 0 {
		for _, issue := range issues {
			_, _, err = client.Notes.CreateIssueNote(issue.ProjectID, issue.IID, &gitlab.CreateIssueNoteOptions{
				Body: func(s string) *string { return &s }(fmt.Sprintf("## Headers\n\n~~~\n%v\n~~~\n\n## Body\n\n~~~\n%s\n~~~\n/close", tc.SystemErr.Data, tc.SystemOut.Data)),
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed to create Gitlab note: %v\n", err)
				return err
			}
		}
	}
	return nil
}
